export default function(editor, opt = {}) {
    const c = opt;
    const domc = editor.DomComponents;
    const defaultType = domc.getType('default');
    const textType = domc.getType('text');
    const defaultModel = defaultType.model;
    const defaultView = defaultType.view;
    let stylePrefix = c.stylePrefix;
    const clsRow = `${stylePrefix}row`;
    const clsCell = `${stylePrefix}cell`;
    const step = 0.2;
    const minDim = 1;
    const currentUnit = 1;
    const resizerBtm = { tl: 0, tc: 0, tr: 0, cl: 0, cr:0, bl:0, br: 0, minDim };
    const resizerRight = { ...resizerBtm, cr: 1, bc: 0, currentUnit, minDim, step };

    if (c.flexGrid) {
        resizerRight.keyWidth = 'flex-basis';
    }

    domc.addType('row', {
        model: defaultModel.extend({
            defaults: {
                ...defaultModel.prototype.defaults,
                resizable: resizerBtm,
            },
        }, {
            isComponent(el) {
                if(typeof el.classList !== 'undefined' && el.classList.contains(clsRow)){
                    return {type: 'row'};
                }
            },
        }),
        view: defaultView,
    });

    domc.addType('cell', {
        model: defaultModel.extend({
            defaults: {
                ...defaultModel.prototype.defaults,
                resizable: resizerRight,
            },
        }, {
            isComponent(el) {
                if(typeof el.classList !== 'undefined' && el.classList.contains(clsCell)){
                    return {type: 'cell'};
                }
            },
        }),
        view: defaultView,
    });
}
