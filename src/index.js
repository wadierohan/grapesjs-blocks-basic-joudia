import grapesjs from 'grapesjs';

export default grapesjs.plugins.add('gjs-blocks-basic', (editor, opts = {}) => {
  const config = {
    blocks: ['row', 'cell', 'column1', 'column2', 'column3', 'column3-7', 'text', 'link', 'image', 'video', 'map'],
    flexGrid: true,
    stylePrefix: 'joudia-',
    addBasicStyle: true,
    category: 'Basic',
    labelRow: 'Row',
    labelCell: 'Cell',
    labelColumn1: '1 Column',
    labelColumn2: '2 Columns',
    labelColumn3: '3 Columns',
    labelColumn37: '2 Columns 3/7',
    labelText: 'Text',
    labelLink: 'Link',
    labelImage: 'Image',
    labelVideo: 'Video',
    labelMap: 'Map',
    ...opts
  };

  // Add components
  const loadComponents = require('./components');
  loadComponents.default(editor, config);

  // Add blocks
  const loadBlocks = require('./blocks');
  loadBlocks.default(editor, config);

});
